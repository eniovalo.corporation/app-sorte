import React, { useState } from 'react';
import { StyleSheet, Text, View } from 'react-native';

export default function Acao() {
    const acoes = [{codigo: 'ITUB4', descricao: 'Itaú'},
      {codigo: 'BBAS3', descricao: 'Banco do Brasil'},
      {codigo: 'BIDI4', descricao: 'Banco Inter'},
      {codigo: 'WEGE3', descricao: 'WEG'},
      {codigo: 'EGIE3', descricao: 'Engie'}
    ];

    const indiceSorteado = Math.floor(Math.random() * acoes.length);

    return (
      <View style={styles.container}>
        <Text style={styles.acao}>{acoes[indiceSorteado].codigo} - {acoes[indiceSorteado].descricao}</Text>
      </View>
    );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  acao: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
});
