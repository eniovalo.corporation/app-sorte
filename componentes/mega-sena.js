import React, { useState } from 'react';
import { StyleSheet, Text, View } from 'react-native';

export default function Megasena() {
    const numerosJogo = gerarNumerosMegassena();

    return (
      <View style={styles.container}>
        <Text style={styles.numero}>{numerosJogo[0]} - {numerosJogo[1]} - {numerosJogo[2]} - {numerosJogo[3]} - {numerosJogo[4]} - {numerosJogo[5]}</Text>
      </View>
    );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  numeros: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
});

function gerarNumerosMegassena() {
  const numerosMega = [];
  const numerosEscolhidos = [];

  for(var numero=1; numero < 61; numero++) {
    numerosMega.push(numero);
  }

  do {
    const indiceSorteado = Math.floor(Math.random() * numerosMega.length);
    numerosEscolhidos.push(numerosMega[indiceSorteado]);

    //Retiro o número já escolhido.
    numerosMega.splice(indiceSorteado, 1);
  } while (numerosEscolhidos.length < 6);

  return numerosEscolhidos;
}