import React, { useState } from 'react';
import { StyleSheet, Text, View } from 'react-native';

export default function Frases() {
    const frases = ['Não esquente a cabeça porque depois vai melhorar!',
        'Dias melhores virão!',
        'Com maiores poderes, maiores responsabilidades!',
        'Viva um dia de cada vez.'
    ];

    const indiceSorteado = Math.floor(Math.random() * frases.length);

    return (
      <View style={styles.container}>
        <Text style={styles.frase}>{frases[indiceSorteado]}</Text>
      </View>
    );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  frase: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
});
