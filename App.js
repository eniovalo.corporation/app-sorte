import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Frases from './componentes/frases';
import Megasena from './componentes/mega-sena';
import Acao from './componentes/acao';

export default class App extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>Minha sorte de hoje!</Text>
        <Frases></Frases>
        <Megasena></Megasena>
        <Acao></Acao>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
});
